#!/bin/bash

# Input
error() {
    message=$1
    echo -e "\033[1;31m> $message\033[0m\n"
    exit -1
}
eval_error() {
    expression=$1
    message=$2
    if [ $expression ]; then
        error $message
    fi
}
show_screen_outputs() {
    screen_outputs=`xrandr | grep connected | cut -d " " -f 1`
    echo -e "> Available screen outputs:"
    for screen_output in $screen_outputs; do
        echo -e "\t- \033[1;32m$screen_output\033[0m "
    done
    echo -ne "? Which screen option do you want: \033[1;34m"
}
changing_mode() {
    screen_output=$1
    mode_name=$2
    echo -e "> Reload screen with new dimensions..."
    xrandr --output $screen_output --mode $mode_name
    echo -e "\033[1;32m> Screen output fixed successful.\033[0m\n"
}   

eval_error "$# -ne 3" "USE: screenfix <screen_width> <screen_heigth> <frame_rate>.\n"
width=$1
heigth=$2
frames=$3
# Get modenline
modenline=`cvt $width $heigth $frames | tail -n +2 | cut -d " " -f 2-`
echo -e  "> The modenline for the dimensions ${width}x${heigth} to $frames(hz) is: \n\t\033[0;35m$modenline\033[0m .\n"

# Set new Mode
echo "> Setting new modenline mode..."
xrandr --newmode $modenline

# Add new mode to specific screen outputcd 
show_screen_outputs
read screen_output
if [ -z $screen_output ]; then
    screen_output="DP-1"
fi
echo -e "\033[0m> You choose the screen output: \033[1;45m $screen_output \033[0m ."
mode_name=`echo $modenline | sed "s/  */ /g" | cut -d " " -f 1`
echo -e "> Adding new mode with name: \033[1;35m$mode_name\033[0m..."

xrandr --addmode $screen_output $mode_name && changing_mode $screen_output $mode_name || error "Adding screen output failed."
