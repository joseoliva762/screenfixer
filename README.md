# Screen Fixer

Xrandr-based program to resize monitors that do not fit properly.


## Instalation 
Download or clone the following gitlab repository and save it in a specific folder for tools.
Repository link: [screenfixer](https://gitlab.com/joseoliva762/screenfixer.git).
```
    git clone https://gitlab.com/joseoliva762/screenfixer.git
    mv ./screenfixer <tools_directory>
    cd <tools_directory>/screenfixer/run
    pwd
```
After placing the program in a directory where can be static, make sure that it has execution permissions for the user. if not, use the command [chmod](https://linux.die.net/man/1/chmod) to change them.

```
    chmod u+x screenfix.sh
```
The **pwd** command used above will be useful for adding the program to the system's **$PATH** environment variable. This is why now, we must move to the home to be able to add it to our bash.
While there it is necessary to open the hidden file **.bashrc** with our favorite text editor, in this case [vim](https://www.vim.org/) will be used.

```
    cd
    vim .bashrc
```
To check the existing path you just have to print the environment variable **$PATH** in this way `echo $PATH`

in the File **.bashrc** we must make sure to add to the existing path the path shown in the **pwd** executed earlier.
> NOTE: Remember that the **pwd path** will be organized, based on the following macro **<root_path>/<tools_directory>/screenfixer/run**, you can also shorten it, writing it like this: **~/<tools_directory>/screenfixer/run**

To add the path to the **$PATH** we must add the following line to the **.bashrc** file, which is responsible for concatenating the previous Path with the new Path. Also, an alias must be registered for easy use.

```
PATH=$PATH:~/<tools_directory>/screenfixer/run
alias screenfix="screenfix.sh"
```
> NOTE: To change to be effective, we must refresh the terminal or, simply execute the command `source .bashrc`.

## USE
Commnad:

- screenfix <screen_width> <screen_heigth> <frames_rate_hz>

Example: 
```
    screenfix 1920 1080 60 
```
During the execution of the program, it will show us all the monitors available in our system, it is imperative to choose one of them, writing its name in the corresponding field. If omitted, the program will default to the `DP-1` option, which is normally used for the system's VGA outputs. In that order of ideas, if the system does not have this option, the program will throw an error.

> NOTE: For more information, please consult the xorg-team article (documentation), which talks about how to use [xrandr](https://xorg-team.pages.debian.net/xorg/howto/use-xrandr.html)